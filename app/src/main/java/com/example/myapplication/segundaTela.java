package com.example.myapplication;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class segundaTela extends AppCompatActivity {
    float peso;
    float altura;
    float imc;
    String valor;
    ImageView foto;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_segunda_tela);
    }

    public void Calcular(View view) {
        final EditText p = (EditText) findViewById(R.id.peso);
        final EditText a = (EditText) findViewById(R.id.altura);
        final ImageView f = (ImageView) findViewById(R.id.imageView2);

        altura = Float.parseFloat(a.getText().toString());
        peso = Float.parseFloat(p.getText().toString());
        imc = (peso) / (altura* altura);






        if (imc <18.5){
            Toast toast = Toast.makeText(getApplicationContext(), "Seu IMC é: " + String.valueOf(imc) + " Abaixo do peso", Toast.LENGTH_LONG);
            toast.show();
            Drawable drawable= getResources().getDrawable(R.drawable.magrinho);
            f.setImageDrawable(drawable);

        }

        if (imc >= 18.6 && imc<25){
            Toast toast = Toast.makeText(getApplicationContext(), "Seu IMC é: " + String.valueOf(imc) + " Peso Ideal", Toast.LENGTH_LONG);
            toast.show();
            Drawable drawable= getResources().getDrawable(R.drawable.magro);
            f.setImageDrawable(drawable);
        }

        if (imc >=25 && imc <30){
            Toast toast = Toast.makeText(getApplicationContext(), "Seu IMC é: " + String.valueOf(imc) + " Levemente acima do peso", Toast.LENGTH_LONG);
            toast.show();
            Drawable drawable= getResources().getDrawable(R.drawable.gordinho);
            f.setImageDrawable(drawable);
        }

        if (imc >=30 && imc <35){
            Toast toast = Toast.makeText(getApplicationContext(), "Seu IMC é: " + String.valueOf(imc) + " Obesidade Grau 1", Toast.LENGTH_LONG);
            toast.show();
            Drawable drawable= getResources().getDrawable(R.drawable.gordo);
            f.setImageDrawable(drawable);
        }

        if (imc >=35 && imc < 40){
            Toast toast = Toast.makeText(getApplicationContext(), "Seu IMC é: " + String.valueOf(imc) + " Obesidade Grau 2", Toast.LENGTH_LONG);
            toast.show();
            Drawable drawable= getResources().getDrawable(R.drawable.gordao);
            f.setImageDrawable(drawable);

        }

        if (imc >=40) {
            Toast toast = Toast.makeText(getApplicationContext(), "Seu IMC é: " + String.valueOf(imc) + " Obesidade Grau 3", Toast.LENGTH_LONG);
            toast.show();
            Drawable drawable= getResources().getDrawable(R.drawable.gordao);
            f.setImageDrawable(drawable);
        }
    }
}
